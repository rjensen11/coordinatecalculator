﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoordinateCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Type out AM_ULN AS_ULN AM_ULW AS_ULW AM_LLN AS_LLN AM_ULW AS_ULW VD HD");
            List<int> values = Console.ReadLine().Split(' ').ToList().Select(s => int.Parse(s)).ToList();
            var upperLeftNorth = new Coordinate(values[0], values[1]);
            var upperLeftWest = new Coordinate(values[2], values[3]);
            var lowerLeftNorth = new Coordinate(values[4], values[5]);
            var upperRightWest = new Coordinate(values[6], values[7]);
            var verticalDivisions = values[8];
            var horizontalDivisions = values[9];

            // first we generate the vertical divisions
            int verticalDifference = upperLeftNorth - lowerLeftNorth;
            int verticalSize = verticalDifference / verticalDivisions;

            List<Coordinate> verticalGuideLines = new List<Coordinate>();
            Coordinate lastCoordinate = upperLeftNorth;
            for (int i = 0; i < verticalDivisions; i++)
            {
                var newGuideLine = lastCoordinate - verticalSize;
                verticalGuideLines.Add(newGuideLine);
                lastCoordinate = newGuideLine;
            }

            // first we generate the horizontal divisions
            int horizontalDifference = upperLeftWest - upperRightWest;
            int horizontalSize = horizontalDifference / horizontalDivisions;

            List<Coordinate> horizontalGuideLines = new List<Coordinate>();
            lastCoordinate = upperLeftWest;
            for (int i = 0; i < horizontalDivisions; i++)
            {
                var newGuideLine = lastCoordinate - horizontalSize;
                horizontalGuideLines.Add(newGuideLine);
                lastCoordinate = newGuideLine;
            }

            FileStream fs = new FileStream(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Coordinates.txt"), FileMode.Create);
            // First, save the standard output.
            TextWriter tmp = Console.Out;
            StreamWriter sw = new StreamWriter(fs);
            Console.SetOut(sw);

            Console.WriteLine("=== Vertical Guides ===");
            verticalGuideLines.ForEach(c => Console.WriteLine(c));

            Console.WriteLine("=== Horizontal Guides ===");
            horizontalGuideLines.ForEach(c => Console.WriteLine(c));

            Console.SetOut(tmp);
            Console.WriteLine("All Done!");
            sw.Close();
            Console.ReadKey(intercept: true);
        }
    }

    public struct Coordinate
    {
        public Coordinate(int arcMinutes, int arcSeconds)
        {
            ArcMinutes = arcMinutes;
            ArcSeconds = arcSeconds;
        }

        public Coordinate(int totalArcSeconds)
        {
            // hack to get around the compiler error about not setting properties first. It doesn't know the dynamic property sets all backing values.
            ArcMinutes = 0;
            ArcSeconds = 0;

            TotalArcSeconds = totalArcSeconds;
        }

        public int ArcMinutes { get; set; }
        public int ArcSeconds { get; set; }
        public int TotalArcSeconds
        {
            get
            {
                return ArcMinutes * 60 + ArcSeconds;
            }
            set
            {
                ArcMinutes = value / 60;
                ArcSeconds = value % 60;
            }
        }

        public static int operator -(Coordinate c1, Coordinate c2)
        {
            return c1.TotalArcSeconds - c2.TotalArcSeconds;
        }

        public static Coordinate operator +(Coordinate c1, int totalArcSeconds)
        {
            return new Coordinate(c1.TotalArcSeconds + totalArcSeconds);
        }

        public static Coordinate operator -(Coordinate c1, int totalArcSeconds)
        {
            return new Coordinate(c1.TotalArcSeconds - totalArcSeconds);
        }

        public override string ToString()
        {
            return $"{ArcMinutes.ToString("00")} {ArcSeconds.ToString("00")}";
        }
    }
}
